import React from 'react';
import { Link } from 'gatsby';
import styles from './BlogPostPreview.module.scss';

export default ({ node }) => {
  console.log('node', node);
  console.log('styles', styles);

  return (
    <div className={styles['blogPostPreview']}>
      <span className={styles['blogPostPreview__date']}>
        {node.frontmatter.date}
      </span>
      <Link to={node.fields.slug}>
        <h2 className={styles['blogPostPreview__heading']}>
          {node.frontmatter.title}
        </h2>
      </Link>
      <p>{node.excerpt}</p>
    </div>
  );
};

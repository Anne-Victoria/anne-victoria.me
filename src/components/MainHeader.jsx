import React from 'react';
import { Link } from 'gatsby';
import styles from './MainHeader.module.scss';

export default () => (
  <header className={styles['mainHeader']}>
    {/* <Link to={`/`}>
      <h3>{data.site.siteMetadata.title}</h3>
    </Link> */}
    <h1>Redesigning Researcher Profiles</h1>
    <Link to={`/about/`}>About</Link>
  </header>
);

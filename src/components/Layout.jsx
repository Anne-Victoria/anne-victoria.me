import React from 'react';
import styles from './Layout.module.scss';

export default ({ children }) => {
  return <div className={styles['layout']}>{children}</div>;
};

import React from 'react';
import { graphql } from 'gatsby';
import Layout from '../components/Layout';
import BlogPostPreview from '../components/BlogPostPreview';
import MainHeader from '../components/MainHeader';

export default ({ data }) => {
  return (
    <Layout>
      <MainHeader />
      <p>{data.allMarkdownRemark.totalCount} Posts</p>
      <hr />
      {data.allMarkdownRemark.edges.map(({ node }) => (
        <BlogPostPreview node={node} key={node.id} />
      ))}
    </Layout>
  );
};

export const query = graphql`
  query {
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "MMM DD, YYYY")
          }
          fields {
            slug
          }
          excerpt
        }
      }
    }
  }
`;
